# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180325205603) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "inventories", force: :cascade do |t|
    t.boolean "blocked"
    t.bigint "survivor_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["survivor_id"], name: "index_inventories_on_survivor_id"
  end

  create_table "items", force: :cascade do |t|
    t.text "item"
    t.integer "price"
    t.integer "quantity"
    t.bigint "inventory_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["inventory_id"], name: "index_items_on_inventory_id"
  end

  create_table "survivors", force: :cascade do |t|
    t.text "name"
    t.integer "age"
    t.integer "gender"
    t.integer "latitude"
    t.integer "longitude"
    t.boolean "infected"
    t.integer "infected_count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "password"
  end

  create_table "trades", force: :cascade do |t|
    t.text "item1"
    t.integer "quantity1"
    t.text "item2"
    t.integer "quantity2"
    t.integer "id1"
    t.integer "id2"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "inventories", "survivors"
  add_foreign_key "items", "inventories"
end
