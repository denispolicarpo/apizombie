class CreateTrades < ActiveRecord::Migration[5.1]
  def change
    create_table :trades do |t|
      t.text :item1
      t.integer :quantity1
      t.text :item2
      t.integer :quantity2
      t.integer :id1
      t.integer :id2

      t.timestamps
    end
  end
end
