class AddPasswordToSurvivor < ActiveRecord::Migration[5.1]
  def change
    add_column :survivors, :password, :text
  end
end
