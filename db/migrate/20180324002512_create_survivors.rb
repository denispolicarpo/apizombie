class CreateSurvivors < ActiveRecord::Migration[5.1]
  def change
    create_table :survivors do |t|
      t.text :name
      t.integer :age
      t.integer :gender
      t.integer :latitude
      t.integer :longitude
      t.boolean :infected
      t.integer :infected_count

      t.timestamps
    end
  end
end
