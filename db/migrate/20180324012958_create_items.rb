class CreateItems < ActiveRecord::Migration[5.1]
  def change
    create_table :items do |t|
      t.text :item
      t.integer :price
      t.integer :quantity
      t.references :inventory, foreign_key: true

      t.timestamps
    end
  end
end
