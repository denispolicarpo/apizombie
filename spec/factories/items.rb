FactoryGirl.define do
  factory :item do
    item { ["Water", "Food", "Medication", "Ammunition"].sample }
    price { [1, 2, 3, 4] }
    inventory 
  end
end
 