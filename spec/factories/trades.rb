FactoryGirl.define do
  factory :trade do
    item1 "MyText"
    quantity1 1
    item2 "MyText"
    quantity2 1
    id1 1
    id2 1
  end
end
