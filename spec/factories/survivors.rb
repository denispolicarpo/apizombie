FactoryGirl.define do
  factory :survivor do
    name { FFaker::Name.name }
    age { rand(1..130) }
    gender { FFaker::Gender.maybe }
    latitude { rand(1..130) }
    longitude { rand(1..130) }
    infected { FFaker::Boolean.maybe }
  end
end
