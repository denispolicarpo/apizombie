class Item < ApplicationRecord
  belongs_to :inventory
  has_many :trades
end
