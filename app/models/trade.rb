class Trade < ApplicationRecord
  def self.trade_survivor(id1, id2, item1_1, qtd1_1, item1_2, qtd1_2,item1_3, qtd1_3, item1_4, qtd1_4, item2_1, qtd2_1, item2_2, qtd2_2, item2_3, qtd2_3, item2_4, qtd2_4)
    @blocked1 = Survivor.find(id1).inventory.blocked
    @blocked2 = Survivor.find(id2).inventory.blocked

    if !(@blocked1 == true && @blocked2 == true)

      items1 = [[item1_1, qtd1_1], [item1_2, qtd1_2], [item1_3, qtd1_3], [item1_4, qtd1_4]]
      items2 = [[item2_1, qtd2_1], [item2_2, qtd2_2], [item2_3, qtd2_3], [item2_4, qtd2_4]]

      a = items1.select do |i, qt|
        unless i.blank? && qt.blank?
          pick_i = Survivor.find(id1).inventory.items.find_by(item: i)
          unless pick_i.blank?
            [i, qt] if pick_i.item == i && pick_i.quantity >= qt
          end
        end
      end

      b = items2.select do |i, qt|
        unless i.blank? && qt.blank?
          pick_i = Survivor.find(id2).inventory.items.find_by(item: i)
          unless pick_i.blank?
            [i, qt] if pick_i.item == i && pick_i.quantity >= qt
          end
        end
      end

      a_price = a.map do |i, q|
        pick = Survivor.find(id1).inventory.items.find_by(item: i)
        (pick.price * q)
      end

      b_price = b.map do |i, q|
        pick = Survivor.find(id2).inventory.items.find_by(item: i)
        (pick.price * q)
      end

      if a_price.reduce(:+) == b_price.reduce(:+)
        a.each do |i, q|
          survivor1 = Survivor.find(id1).inventory.items.find_by(item: i)
          survivor1.update(quantity: (survivor1.quantity - q))
        end

        b.each do |i, q|
          survivor1 = Survivor.find(id1).inventory.items.find_by(item: i)
          survivor1.update(quantity: (survivor1.quantity + q))
        end

        b.each do |i, q|
          survivor2 = Survivor.find(id2).inventory.items.find_by(item: i)
          survivor2.update(quantity: (survivor2.quantity - q))
        end

        a.each do |i, q|
          survivor2 = Survivor.find(id2).inventory.items.find_by(item: i)
          survivor2.update(quantity: (survivor2.quantity + q))
        end
        return Survivor.find(id1).inventory.items, Survivor.find(id2).inventory.items
      else
        'A troca só é permitida com itens em estoque e de valores equivalentes'
      end
    else
      'A troca só é permitida entre sobreviventes não infectados!'
    end
  end
end
