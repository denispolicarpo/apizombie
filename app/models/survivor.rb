class Survivor < ApplicationRecord
  has_one :inventory

  validates :name, :age, :gender, :latitude, :longitude, :password, presence: true, on: :create
  enum gender: %i[male female]

  after_create :create_inventory
  before_create :set_infected
  after_update :block_inventory

  def set_infected
    self.infected = false
    self.infected_count = 0
  end

  def create_inventory
    Inventory.create(survivor: self)
  end

  def block_inventory
    if self.infected == true
      inventory = Inventory.find(self.id)
      inventory.update(blocked: true)
    end
  end

  def self.estimatives
    @survivor_infected = Survivor.where(infected: true)
    @water = []
    @water_price = []
    @food = []
    @food_price = []
    @medication = []
    @medication_price = []
    @ammunition = []
    @ammunition_price = []
    @survivor_infected.each do |s|
      @water << s.inventory.items.where(item: 'Water').pluck(:quantity).join.to_i
      @water_price << s.inventory.items.where(item: 'Water').pluck(:price).join.to_i
      @food << s.inventory.items.where(item: 'Food').pluck(:quantity).join.to_i
      @food_price << s.inventory.items.where(item: 'Food').pluck(:price).join.to_i
      @medication << s.inventory.items.where(item: 'Medication').pluck(:quantity).join.to_i
      @medication_price << s.inventory.items.where(item: 'Medication').pluck(:price).join.to_i
      @ammunition << s.inventory.items.where(item: 'Ammunition').pluck(:quantity).join.to_i
      @ammunition_price << s.inventory.items.where(item: 'Ammunition').pluck(:price).join.to_i
    end
    if !(@water.blank? && @water_price.blank? && @food.blank? && @food_price.blank? && @medication.blank? && @medication_price.blank? && @ammunition.blank? && @ammunition_price.blank?)
      ((@water.reduce(:+) * @water_price.reduce(:+)) + (@food.reduce(:+) * @food_price.reduce(:+)) + (@medication.reduce(:+) * @medication_price.reduce(:+)) + (@ammunition.reduce(:+) * @ammunition_price.reduce(:+)))
    else
      'Não há infectados'
    end
  end
end
