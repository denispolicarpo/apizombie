class Inventory < ApplicationRecord
  has_many :items
  belongs_to :survivor

  before_create :set_blocked
  after_create :create_items

  def set_blocked
    self.blocked = false
  end

  def create_items
    items << Item.create(item: 'Water', price: 4, quantity: 1)
    items << Item.create(item: 'Food', price: 3, quantity: 1)
    items << Item.create(item: 'Medication', price: 2, quantity: 1)
    items << Item.create(item: 'Ammunition', price: 1, quantity: 1)
  end

  def self.average
    @inventories = Inventory.all
    @water = []
    @food = []
    @medication = []
    @ammunition = []
    @inventories.each do |inventory|
      @water << inventory.items.where(item: 'Water').pluck(:quantity).join.to_i
      @food << inventory.items.where(item: 'Food').pluck(:quantity).join.to_i
      @medication << inventory.items.where(item: 'Medication').pluck(:quantity).join.to_i
      @ammunition << inventory.items.where(item: 'Ammunition').pluck(:quantity).join.to_i
    end

    [
      "Water average: #{(@water.reduce(:+) / @inventories.count)}",
      "Food average: #{(@food.reduce(:+) / @inventories.count)}",
      "Medication average: #{(@medication.reduce(:+) / @inventories.count)}",
      "Ammunition average: #{(@ammunition.reduce(:+) / @inventories.count)}"
    ]
  end

end
