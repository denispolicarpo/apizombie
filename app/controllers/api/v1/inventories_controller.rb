class Api::V1::InventoriesController < ApplicationController
  def index
    @average = Inventory.average

    render json: { average: @average }
  end
end
