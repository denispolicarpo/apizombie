class Api::V1::SurvivorsController < ApplicationController
  def index
    @survivor = Survivor.all
    @survivor_infected = Survivor.where(infected: true)
    render json: { infected: "#{((@survivor_infected.count * 100) / @survivor.count)}%",
                   non_infected: "#{((@survivor_infected.rewhere(infected: false).count * 100) / @survivor.count)}%",
                   points_lost: @survivor.estimatives }
  end

  def create
    @survivor = Survivor.create(survivor_params)
    if @survivor.save
      render json: @survivor
    else
      render json: { message: 'error' }
    end
  end

  def update
    @find_survivor = Survivor.find(params[:id])
    @find_survivor = Survivor.find(params[:id])
    if !(params[:infected].blank? || params[:infected] == false)
      infected_qt = Survivor.find(params[:id]).infected_count
      infected_qt += 1
      if infected_qt < 3
        if @find_survivor.update(infected_count: infected_qt)
          render json: @find_survivor
        else
          render json: { message: 'error' }
        end
      elsif @find_survivor.update(infected: params[:infected], infected_count: infected_qt)
        render json: @find_survivor
      else
        render json: { message: 'error' }
      end
    elsif @find_survivor.password == params[:password]
      @find_survivor.update(latitude: params[:latitude], longitude: params[:longitude])
      render json: @find_survivor
    else
      render json: { message: 'senha incorreta' }
    end
  end

  private

  def survivor_params
    params.require(:survivor).permit(:name, :age, :gender, :latitude, :longitude,
                                     :infected, :infected_count, :password)
  end
end
