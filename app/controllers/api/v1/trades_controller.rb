class Api::V1::TradesController < ApplicationController
  def create
    @trade = Trade.trade_survivor(params[:id1], params[:id2], params[:item1_1], 
                                  params[:qtd1_1], params[:item1_2], params[:qtd1_2], 
                                  params[:item1_3], params[:qtd1_3], params[:item1_4], 
                                  params[:qtd1_4], params[:item2_1], params[:qtd2_1], 
                                  params[:item2_2], params[:qtd2_2], params[:item2_3], 
                                  params[:qtd2_3], params[:item2_4], params[:qtd2_4])
    render json: { survivors: @trade }
  end

  def trade_params
    params.require(:trade).permit(:item1_1, :item1_2, :item1_3, :item1_4, 
                                  :item2_1, :item2_2, :item2_3, :item2_4, 
                                  :qtd1_1, :qtd1_2, :qtd1_3, :qtd1_4, 
                                  :qtd2_1, :qtd2_2, :qtd2_3, :qtd2_4, :id1, :id2)
  end
end
